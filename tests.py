import pyphen
import unittest


patterns = {
    'aujourd’hui': 'au-jour-d’hui',
    'soixante': 'soi-xan-te',
    'bruxelles': 'bru-xel-les',
    'plastique': 'plas-ti-que',
    'cherchent': 'cher-chent',
    'caractéristiques': 'ca-rac-té-ris-ti-ques',
    'paraissent': 'pa-rais-sent',
    'femmes': 'fem-mes',
    'recherche': 're-cher-che',
    'logique': 'lo-gi-que',
    'Politique': 'Po-li-tique',
    'prévoyant': 'pré-voyant',
    'tribunal': 'tri-bu-nal',
}


class MyTest(unittest.TestCase):
    def setUp(self):
        self.dic = pyphen.Pyphen(filename='hyph_fr_FR.dic', left=2, right=2)

    def test(self):
        for k, v in patterns.items():
            with self.subTest(k=k, v=v):
                self.assertEqual(self.dic.inserted(k), v)



if __name__ == '__main__':
    unittest.main()
